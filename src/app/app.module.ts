import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { DoubleBracketModule } from './double-bracket/double-bracket.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DoubleBracketModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
