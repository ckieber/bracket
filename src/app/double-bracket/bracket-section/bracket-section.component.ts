import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { Competitor } from '../model/competitor';
import { BracketConnectionComponent } from '../bracket-connection/bracket-connection.component';

@Component({
  selector: 'app-bracket-section',
  templateUrl: './bracket-section.component.html',
  styleUrls: ['./bracket-section.component.css']
})
export class BracketSectionComponent implements OnInit {
  @Input() connectionHeight: number;
  @Input() connectionWidth: number;
  @Input() nameWidth: number;
  @Input() badgeWidth: number;
  @Input() topCompetitor: Competitor;
  @Input() bottomCompetitor: Competitor;
  @Input() highlightId: number;
  @Output() mouseOver: EventEmitter<number> = new EventEmitter<number>();
  @Output() mouseLeave: EventEmitter<number> = new EventEmitter<number>();
  @Output() advance: EventEmitter<Competitor> = new EventEmitter<Competitor>();
  @ViewChild(BracketConnectionComponent) connectionComponent: BracketConnectionComponent;

  constructor() { }

  ngOnInit() {

  }

  onMouseOver(event: number): void {
    this.mouseOver.emit(event);
  }

  onMouseLeave(): void {
    this.mouseLeave.emit();
  }

  onAdvance(event: Competitor): void {
    if (this.topCompetitor.id === event.id) {
      this.connectionComponent.setTopWinner(event.id);
      this.advance.emit(this.topCompetitor);
    }
    else if (this.bottomCompetitor.id === event.id) {
      this.connectionComponent.setBottomWinner(event.id);
      this.advance.emit(this.bottomCompetitor);
    }
  }
}
