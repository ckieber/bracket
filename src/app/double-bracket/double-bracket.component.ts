import { Component, OnInit } from '@angular/core';
import { Competitor } from './model/competitor'
import { Section } from './model/section';

@Component({
  selector: 'app-double-bracket',
  templateUrl: './double-bracket.component.html',
  styleUrls: ['./double-bracket.component.css']
})
export class DoubleBracketComponent implements OnInit {
  rounds: number[];
  sections: Section[][];
  highlightId: number;

  constructor() {
    this.sections = [[
      new Section(new Competitor(1, 'team 1ffsadfsdfdsafdsfdsafdafdasfdsf', 'last'), new Competitor(2, 'team 2', 'last')),
      new Section(new Competitor(3, 'team 3', 'last'), new Competitor(4, 'team 4', 'last')),
      new Section(new Competitor(5, 'team 5', 'last'), new Competitor(6, 'team 6', 'last')),
      new Section(new Competitor(7, 'team 7', 'last'), new Competitor(8, 'team 8', 'last'))
    ],
    [
      new Section(new Competitor(1, 'team 1', 'last'), new Competitor(2, 'team 2', 'last')),
      new Section(new Competitor(3, 'team 3', 'last'), new Competitor(4, 'team 4', 'last')),
    ],
    [
      new Section(null, new Competitor(2, 'team 2', 'last'))
    ]];

    this.rounds = Array<number>(this.sections.length).fill(1).map((x, i) => i)
  }

  ngOnInit() {
    
  }

  getHeight(round: number): number {
    return Math.pow(2, round)*75;
  }

  setHighlightId(event: number) {
    this.highlightId = event;
  }

  removeHighlightId() {
    this.highlightId = null;
  }

  onAdvance(event: Competitor) {
    console.log(event.id);
    //find last pos of id
    //calculate new id
    //add to section at id
  }

}
