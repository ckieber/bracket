import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Competitor } from '../model/competitor';

@Component({
  selector: 'app-competitor-edit-modal',
  templateUrl: './competitor-edit-modal.component.html',
  styleUrls: ['./competitor-edit-modal.component.css']
})
export class CompetitorEditModalComponent implements OnInit {
  @Input() competitor: Competitor;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  addWin() {
    this.competitor.wins++;
  }

  advance() {
    this.activeModal.close('advance');
  }

}
