import { Competitor } from './competitor';

export class Section {
  
  constructor(public topCompetitor: Competitor, public bottomCompetitor: Competitor) { }
}