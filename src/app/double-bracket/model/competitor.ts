export class Competitor {
  wins: number;

  constructor(public id: number, public firstName: string, public lastName: string) {
    this.wins = 0;
  }
}