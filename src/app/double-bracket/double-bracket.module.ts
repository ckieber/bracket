import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DoubleBracketComponent } from './double-bracket.component';
import { BracketCompetitorComponent } from './bracket-competitor/bracket-competitor.component';
import { BracketConnectionComponent } from './bracket-connection/bracket-connection.component';
import { BracketSectionComponent } from './bracket-section/bracket-section.component';
import { CompetitorEditModalComponent } from './competitor-edit-modal/competitor-edit-modal.component';

@NgModule({
  imports: [
    NgbModule.forRoot(),
    CommonModule
  ],
  declarations: [
    DoubleBracketComponent,
    BracketCompetitorComponent,
    BracketConnectionComponent,
    BracketSectionComponent,
    CompetitorEditModalComponent
  ],
  exports: [
    DoubleBracketComponent
  ],
  entryComponents: [CompetitorEditModalComponent]
})
export class DoubleBracketModule { }