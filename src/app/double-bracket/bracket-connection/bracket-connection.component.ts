import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bracket-connection',
  templateUrl: './bracket-connection.component.html',
  styleUrls: ['./bracket-connection.component.css']
})
export class BracketConnectionComponent implements OnInit {
  @Input() height: number;
  @Input() width: number;
  @Input() highlightId: number;
  private topId: number;
  private bottomId: number;
  private winnerId: number;

  constructor() {
    this.topId = -1;
    this.bottomId = -1;
    this.winnerId = -1;
  }

  ngOnInit() {

  }

  setTopWinner(id: number) {
    this.topId = id;
    this.winnerId = id;
  }

  setBottomWinner(id: number) {
    this.bottomId = id;
    this.winnerId = id;
  }
}
