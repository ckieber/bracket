import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { CompetitorEditModalComponent } from '../competitor-edit-modal/competitor-edit-modal.component';
import { Competitor } from '../model/competitor';

@Component({
  selector: 'app-bracket-competitor',
  templateUrl: './bracket-competitor.component.html',
  styleUrls: ['./bracket-competitor.component.css']
})
export class BracketCompetitorComponent implements OnInit {
  @Input() nameWidth: number;
  @Input() badgeWidth: number;
  @Input() competitor: Competitor;
  @Input() highlightId: number;
  @Output() mouseOver: EventEmitter<number> = new EventEmitter<number>();
  @Output() mouseLeave: EventEmitter<number> = new EventEmitter<number>();
  @Output() advance: EventEmitter<Competitor> = new EventEmitter<Competitor>();

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  onMouseOver(): void {
    if (this.competitor) {
      this.mouseOver.emit(this.competitor.id);
    }
  }

  onMouseLeave(): void {
    this.mouseLeave.emit();
  }

  openEditModal() {
    if (!this.competitor) {
      return;
    }

    const modalRef = this.modalService.open(CompetitorEditModalComponent);
    modalRef.componentInstance.competitor = this.competitor;
    modalRef.result.then((result) => {
      if (result === 'advance') {
        this.advance.emit(this.competitor)
      }
    }, (reason) => {

    });
  }
}
