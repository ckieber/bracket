import { BracketPage } from './app.po';

describe('bracket App', () => {
  let page: BracketPage;

  beforeEach(() => {
    page = new BracketPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
